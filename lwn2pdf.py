#!/usr/bin/env python3

"""lwn2pdf.

This is a small script that extracts LWN articles, converts them to PDF and
sends them to the user. It was born at 2 am one morning when I was too annoyed
that I couldn't read and annotate LWN's excellent in-depth articles on my
tablet.

Author: Darshit Shah <git@darnir.net>
License: GPLv3+
"""

import configparser
import os
import subprocess
import sys

import feedparser
from bs4 import BeautifulSoup


def get_articles():
    """Get all recent articles from the LWN Feed

    Args: None
    Returns: A Generator returning a tuple of (article_title, article_number, uri)
    """
    feed = feedparser.parse("https://lwn.net/headlines/Features")
    for article in feed.entries:
        if article.title.startswith('[$] '):
            title = article.title[4:]
        else:
            title = article.title
        # Get the URI, but without the /rss path component
        uri = "/".join(article.link.split("/")[:-1])
        article_number = article.link.split("/")[-2]
        yield (title, article_number, uri)


def scrape(soup):
    """Scrape a article HTML to get only the main article text.

    This method is like readability, but was written only to work on LWN.net's
    pages. It's very hacky and will break if they change the HTML DOM. Always
    willing to fix / imprive this, but it works just fine for me right now.

    Args: A bs4 soup of a LWN article
    Returns: None. Modifies soup to delete all cruft.
    """

    # Remove all script tags
    while soup.script:
        soup.script.extract()

    # Remove extra link tags
    for i in soup.find_all("link"):
        if "stylesheet" not in i["rel"]:
            i.extract()

    body = soup.body

    body.center.extract()

    for div in body.find_all("div", id="menu"):
        div.extract()

    for div in body.find_all("div", recursive=False):
        if "pure-grid" not in div.attrs["class"]:
            div.extract()
        else:
            mbody = div

    for div in mbody.find_all("div", recursive=False):
        if "not-print" in div.attrs["class"]:
            div.extract()

    for div in mbody.div.find_all("div", recursive=False):
        if "Comment" in div.attrs["class"] or "CommentBox" in div.attrs["class"]:
            div.extract()

    while mbody.div.table:
        mbody.div.table.extract()


def check_matrix_logged_in(cookie):
    """Sanity check that we are logged-in to a Matrix account.

    There is no point in doing any of the work if the PDFs cannot be pushed to
    the device. So check that we have a valid login.

    Args: cookie. Path to matrix cookie file. (Optional)
    """
    try:
        subprocess.run(['matrix-whoami', cookie], check=True)
    except FileNotFoundError:
        print("matrix-whoami not found. Cannot continue")
        sys.exit(1)
    except subprocess.CalledProcessError:
        print("Seems like the user is not logged in. Exiting")
        sys.exit(1)


def load_config():
    """Parse the config file and load it into a in-memory dictionary."""
    conf = dict()
    config = configparser.ConfigParser()
    config.read(os.path.join(os.getcwd(), 'lwn2pdf.ini'))
    try:
        conf['roomid'] = config['matrix']['room']
    except KeyError:
        print("key \"room\" not found in [matrix] in configuration")
        sys.exit(1)

    try:
        conf['wget_cookie'] = config['wget']['cookie']
    except KeyError:
        print("key \"cookie\" not found in [wget] in configuration")
        sys.exit(1)

    try:
        conf['mss_cookie'] = config['matrix']['cookie']
    except KeyError:
        conf['mss_cookie'] = ''

    return conf


if __name__ == "__main__":
    config = load_config()
    check_matrix_logged_in(config['mss_cookie'])

    destdir = os.path.join(os.getcwd(), "lwn2pdf")
    pdfdir = os.path.join(destdir, "pdf")
    htmldir = os.path.join(destdir, "html")
    os.makedirs(pdfdir, exist_ok=True)
    os.makedirs(htmldir, exist_ok=True)

    for title, num, url in get_articles():
        print("* Handling: [" + num + "] " + title + " (" + url + ")")
        article_path = os.path.join(htmldir, num+".html")
        if os.path.exists(article_path):
            print("  * [SKIPPING]: " + title)
            continue
        print("  * Scraping Article: " + title)
        html_path = os.path.join(htmldir, title+".html")
        pdf_path = os.path.join(pdfdir, title+".pdf")

        subprocess.run(['wget',
                        '--no-config',
                        '--convert-links',
                        '--quiet', '--show-progress',
                        '--load-cookies', config['wget_cookie'],
                        '--save-cookies', config['wget_cookie'],
                        '-O', article_path, url], check=True)

        if not os.path.exists(article_path):
            print("Error downloading article")
            sys.exit(1)

        with open(article_path, 'r') as fp:
            soup = BeautifulSoup(fp, features='lxml')

        print("    * Cleaning HTML")
        scrape(soup)

        with open(html_path, 'w') as fp:
            print("    * Writing HTML")
            fp.write(soup.prettify())

        print("    * Creating PDF")
        subprocess.run(['wkhtmltopdf', html_path, pdf_path], check=True)

        print("    * Sending PDF")
        try:
            subprocess.run(["matrix-send-file",
                            config['roomid'],
                            pdf_path,
                            config['mss_cookie']],
                           check=True)
        except FileNotFoundError:
            print("matrix-send-file not found. Cannot continue")
            sys.exit(1)
        except subprocess.CalledProcessError:
            print("Error sending PDF")
            sys.exit(1)
