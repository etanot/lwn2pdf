# lwn2pdf

This is a small script that extracts LWN articles, converts them to PDF and
sends them to the user. It was born at 2 am one morning when I was too annoyed
that I couldn't read and annotate LWN's excellent in-depth articles on my
tablet.

It uses the logged in user's own CSS for the LWN articles, so you always feel
comfortable reading them.

## Requirements

The following Python packages need to be installed for the script to work:
* BeautifulSoup4
* feedparser

Apart from Python, this script also requires that
[matrix-shell-suite](https://gitlab.com/darnir/matrix-shell-suite) is
available. Remember to add its `src/` directory to the `$PATH` variable since
MSS doesn't currently have an "install" option. MSS requires:
* POSIX sh
* curl
* jq

## Configuration

The script uses a simple ini-style configuration file and accepts **no**
command-line parameters. The script is super-dumb (Remember, I wrote it to
scratch an itch at 2am), and expects the configuration file in the `$PWD` where
it was invoked from.

A Sample configuration with all available options is shipped in the repository.
Only the `matrix.cookie` parameter is optional. The Matrix Shell Suite defaults
to searching for a cookie in
`${XDG_DATA_HOME:-$HOME/.local/share}/mss/matrix.cookie`.

## Extras

In the `contrib/` directory, I've added a few extras that I used in my personal
deployment of this script. These extras include a Dockerfile for building a
self-contained Docker container and some Systemd unit files used for running it
regularly.

To use the extras, first edit the configuration file in the repo to have all the
correct values. Then, build the container using:

```
$ docker build . -t lwn2pdf -f contrib/Dockerfile
```

This container can then be run as:

```
$ docker run -it -v /path/to/lwn2pdf-volume:/lwn2pdf lwn2pdf
```

These commands can also be seen in the Systemd units which set up a daily timer to
send over the new articles fresh every morning.

## Author
Darshit Shah <git@darnir.net>

Bug reports, suggestions, improvements, etc. are encouraged via GitLab issues.

## License
GPLv3+
